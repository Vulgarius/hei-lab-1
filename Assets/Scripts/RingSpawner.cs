﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RingSpawner : MonoBehaviour
{

    private int _spawnCount;
    
    private Vector3 _newSpawn;
    
    private GameObject _1Ring;
    private GameObject _2Ring;
    private GameObject _newRing;
    private GameObject _oldRing;
    private Transform _firstRing;

    private int _rotationSpeedNew, _rotationSpeedCurrent;

    private int _random;

    public GameObject[] spawnArray;
    
    public GameObject prefab;
    public GameObject current;
    public PlayerBehaviour playerBehaviour;

    public bool mayI;
    
    private void Start()
    {
        mayI = true;
        _newSpawn = new Vector3(0,5,0);
        _firstRing = current.transform;
        playerBehaviour.target = _firstRing;
        spawnArray = new GameObject[3];
    }

    // Update is called once per frame
    void Update()
    {
        if (mayI)
        {
            while (_spawnCount < 3)
            {
                _spawnCount++;

                _newSpawn.x = Random.Range(-2, 2);
                
                _newRing = Instantiate(prefab, current.transform.position + _newSpawn, current.transform.rotation);

                spawnArray[_spawnCount - 1] = _newRing;
                
                _rotationSpeedNew = Random.Range(30, 60);
                _rotationSpeedCurrent = Random.Range(30, 60);
                
                _newRing.GetComponent<RingBehaviour>().rotationSpeed = _rotationSpeedNew;
                current.GetComponent<RingBehaviour>().rotationSpeed = _rotationSpeedCurrent;
                
                current.GetComponent<RingBehaviour>().targetring = _newRing.transform;
                current = _newRing;
            }
        }

        for (var i = 0; i < 2; i++)
        {
            if (spawnArray[i].GetComponent<RingBehaviour>().rotationSpeed ==
                spawnArray[i + 1].GetComponent<RingBehaviour>().rotationSpeed)
            {
                spawnArray[i + 1].GetComponent<RingBehaviour>().rotationSpeed = Random.Range(20, 30);
            }
        }
        
        if (_spawnCount == 3)
        {
            mayI = !mayI;
            _spawnCount = 0;
        }
    }
}
