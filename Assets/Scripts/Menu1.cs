using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu1 : MonoBehaviour
{

    public GameObject menu1, chronic, pain, relief, disclaimer, menu3;
    
    // Start is called before the first frame update
    void Start()
    {
        menu1.SetActive(true);
        StartCoroutine(Chronic());
    }
    
    private IEnumerator Chronic()
    {
        chronic.SetActive(true);
        chronic.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(3);
        chronic.GetComponent<FadeIn>().enabled = false;
        chronic.GetComponent<FadeIn>()._mFaded = false;
        StartCoroutine(DisclaimerIn());
    }
    private IEnumerator DisclaimerIn()
    {
        chronic.SetActive(false);
        pain.SetActive(false);
        relief.SetActive(false);
        
        disclaimer.SetActive(true);
        disclaimer.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(7);
        disclaimer.GetComponent<FadeIn>().enabled = false;
        disclaimer.GetComponent<FadeIn>()._mFaded = false;

        StartCoroutine(DisclaimerOut());
    }
    private IEnumerator DisclaimerOut()
    {
        disclaimer.SetActive(true);
        disclaimer.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        disclaimer.GetComponent<FadeOut>().enabled = false;
        disclaimer.GetComponent<FadeOut>()._mFaded = false;
        StartCoroutine(MenuGame());
    }
    private IEnumerator MenuGame()
    {
        menu1.SetActive(false);

        menu3.SetActive(true);
        menu3.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(1);
        menu3.GetComponent<FadeIn>().enabled = false;
        menu3.GetComponent<FadeIn>()._mFaded = false;
    }
    
}
