﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject[] orbs;
    public GameObject[] level;
    public GameObject afterPain, exitGame;
    public GameObject painQuestion, fim;
    public GameObject buttonsAndText;
    public int selected;
    public int exitSelected;

    private int _totalPoints;
    private bool _select = false;
    private bool _exit = false;
    private int _questionCount = 0;
    private Animator _bat;
    private int _random;
    public float timer;
    [SerializeField]private AfterGameQuest[] gameQuestArray = new AfterGameQuest[7];
    [SerializeField] private int gameQuestIndex = 0;
    [System.Serializable]
    public class AfterGameQuest
    {
        public GameObject QuestionParent;
        public Animator QuestionParentAnimator;
        public bool HasAnswered = false;
        public GameObject NextButton;
        public int Selected;
    }
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        
        if (_select)
        {
            Selection();
        }

        if (_questionCount == 10)
        {
            fim.SetActive(true);
        }

        if (_exit)
        {
            Selection();
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitCommand();
        }
    }
    
    private void Start()
    {
        gameQuestIndex = 0;
        Screen.orientation = ScreenOrientation.Portrait;
        _bat = buttonsAndText.GetComponent<Animator>();
        StartCoroutine(Question());
        InvokeRepeating(nameof(StartQuestion),300,300);
        
        _random = Random.Range(0, 5);
        //orbs[_random].SetActive(true);
        
    }

    public void QuitCommand()
    {
        StartCoroutine(Question());
        _bat.ResetTrigger("Idle");
        _exit = true;
    }
    
    public void Selection()
    {
        for (int i = 0; i < 10; i++)
        {
            if (level[i] == EventSystem.current.currentSelectedGameObject)
            {
                
                if (_exit)
                {
                    exitSelected = i;
                    exitGame.SetActive(true);
                    _bat.SetTrigger("Move");
                }
                else
                {
                    selected = i;
                    afterPain.SetActive(true);
                    _bat.ResetTrigger("Idle");
                    _bat.SetTrigger("Move");
                }
                    
            }
        }
    }

    public void AddCount()
    {
        _questionCount++;
    }
    
    public void Quit()
    {
        SceneManager.LoadScene("BackMenu");
    }
    
    public void StartQuestion()
    {
        StartCoroutine(Question());
    }
    private IEnumerator Question()
    {
        painQuestion.SetActive(true);
        painQuestion.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeIn>().enabled = false;
        painQuestion.GetComponent<FadeIn>()._mFaded = false;
        _select = true;
    }
    
    public void ToGame()
    {
        StartCoroutine(Game());
    }
    private IEnumerator Game()
    {
        _select = false;
        afterPain.SetActive(false);
        painQuestion.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeOut>().enabled = false;
        painQuestion.GetComponent<FadeOut>()._mFaded = false;
        _bat.ResetTrigger("Move");
        _bat.SetTrigger("Idle");
        painQuestion.SetActive(false);
    }
    //Called by buttons
    public void AfterGameQuestionButtonSelected(int index)
    {
        gameQuestArray[gameQuestIndex].Selected = index;
        if (!gameQuestArray[gameQuestIndex].HasAnswered)
        {
            gameQuestArray[gameQuestIndex].HasAnswered = true;
            gameQuestArray[gameQuestIndex].QuestionParentAnimator.ResetTrigger("Idle");
            gameQuestArray[gameQuestIndex].QuestionParentAnimator.SetTrigger("Move");
            gameQuestArray[gameQuestIndex].NextButton.SetActive(true);
        }
    }
    public void AddGameQuestIndex()
    {
        gameQuestArray[gameQuestIndex].QuestionParent.gameObject.SetActive(false);
        gameQuestIndex++;
        DisplayQuest();
    }
    public void DisplayQuest()
    {
        gameQuestArray[gameQuestIndex].QuestionParent.gameObject.SetActive(true);
    }
    public AfterGameQuest[] GetAfterGameQuest()
    {
        return gameQuestArray;
    }
    
}
