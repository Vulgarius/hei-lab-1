﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    private Transform _player;
    public float speed;

    public float camPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if((_player.position.y + camPosition) != transform.position.y)
        {
            var position = _player.position;
            var transform1 = transform;
            transform1.position = new Vector3(position.x,position.y + camPosition, transform1.position.z);
        }
    }
}
