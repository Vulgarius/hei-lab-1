using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instruction : MonoBehaviour
{
    public GameObject instruction;
    
    // Start is called before the first frame update
    public void DoIt()
    {
        StartCoroutine(MenuGame());
    }
    
    private IEnumerator MenuGame()
    {
        yield return new WaitForSeconds(10);
        instruction.SetActive(true);
        instruction.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(10);
        instruction.GetComponent<FadeIn>().enabled = false;
        instruction.GetComponent<FadeIn>()._mFaded = false;
        
        instruction.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        instruction.GetComponent<FadeOut>().enabled = false;
        instruction.GetComponent<FadeOut>()._mFaded = false;
        
        instruction.SetActive(false);
    }
}
