using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SendToGoogle : MonoBehaviour
{
    public GameManager gamemanager;
    public static string deviceUniqueIdentifier;
    public PlayerMovement playerMovement;

    private string _name;
    private string[] _level = new string[99];
    private int _index = 0;

    [SerializeField] private string baseURL =
        "https://docs.google.com/forms/u/0/d/e/1FAIpQLScQ3aU7ophasgzvnCHQjP1Z7YsUswP6lI0b3qgkdXRHBaLr0Q/formResponse";

    private void Start()
    {
        for (int i = 0; i < 99; i++)
        {
            _level[i] = "N/C";
        }
    }

    IEnumerator Post(string[] levels)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.1735345370", SceneManager.GetActiveScene().name);
        form.AddField("entry.1334737443", SystemInfo.deviceUniqueIdentifier);
        form.AddField("entry.1474747466", levels[0]);
        form.AddField("entry.108284582", levels[1]);
        form.AddField("entry.384563860",levels[2]);
        form.AddField("entry.1473429139", levels[3]);
        form.AddField("entry.229304459", levels[4]);
        form.AddField("entry.1767885312",levels[5]);
        form.AddField("entry.30626050", levels[6]);
        form.AddField("entry.1535130346", levels[7]);
        if(SceneManager.GetActiveScene().name == "Jogo")
            form.AddField("entry.953337149","N/C");
        else
            form.AddField("entry.953337149",playerMovement._orbs);
        form.AddField("entry.1773755946",TimeSpan.FromSeconds(gamemanager.timer).Minutes + "m" + TimeSpan.FromSeconds(gamemanager.timer).Seconds + "s");
        //New questions
        var gameQuestArray = gamemanager.GetAfterGameQuest();
        form.AddField("entry.2095229730", gameQuestArray[0].Selected);
        form.AddField("entry.833480925", gameQuestArray[1].Selected);
        form.AddField("entry.54349716", gameQuestArray[2].Selected);
        form.AddField("entry.1673928452", gameQuestArray[3].Selected);
        form.AddField("entry.1880278131", gameQuestArray[4].Selected);
        form.AddField("entry.5831418", gameQuestArray[5].Selected);
        form.AddField("entry.2058644690", gameQuestArray[6].Selected);
        byte[] rawData = form.data;
        WWW www = new WWW(baseURL, rawData);
        yield return www;
    }

    public void FillStart()
    {
        _level[_index] = Convert.ToString(gamemanager.selected+1);
        _index++;
    }

    public void FillExit()
    {
        _level[_index] = Convert.ToString(gamemanager.exitSelected+1);
    }
    
    public void Send()
    {
        StartCoroutine(Post(_level));
    }
    
}
