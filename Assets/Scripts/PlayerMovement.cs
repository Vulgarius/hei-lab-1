﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController controller;
    public Transform groundCheck;

    public float speed = 3f;
    public float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public GameObject score;
    public Text scoreText;

    private AudioSource _audioSource;
    private Vector3 velocity;
    private Vector3 move;
    private bool isGrounded;
    public int _orbs;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        move = transform.forward * 1;

        if (SystemInfo.supportsGyroscope)
        {
            if (Input.GetMouseButton(0))
            {
                StartCoroutine(Walk());
            }
        }
        else
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
        
            Vector3 move = transform.right * x + transform.forward * z;
            controller.Move(move * speed * Time.deltaTime);
        }
        
        

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }

    private IEnumerator Walk()
    {
        controller.Move(move * speed * Time.deltaTime);
        yield return new WaitForSeconds(2f);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ORB2"))
        {
            Destroy(other.gameObject);
            _audioSource.Play();
            _orbs++;
            scoreText.text = _orbs + "/10";
            StartCoroutine(PlusScore());
        }
    }
    
    private IEnumerator PlusScore()
    {
        score.SetActive(true);
        score.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(score.GetComponent<FadeIn>().duration);
        score.GetComponent<FadeIn>().enabled = false;
        score.GetComponent<FadeIn>()._mFaded = false;
        
        yield return new WaitForSeconds(3);
        
        score.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(score.GetComponent<FadeIn>().duration);
        score.GetComponent<FadeOut>().enabled = false;
        score.GetComponent<FadeOut>()._mFaded = false;
        score.SetActive(false);
    }
    
}
