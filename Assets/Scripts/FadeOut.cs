﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public bool _mFaded;
    public float duration;
    public GameObject titleMenu;
    public void Update()
    {
        if (_mFaded == false && titleMenu) { 
            var canvGroup = GetComponent<CanvasGroup>();
            StartCoroutine(DoFade(canvGroup, canvGroup.alpha, _mFaded ? 1 : 0));
            _mFaded = !_mFaded;
        }
    }

    public IEnumerator DoFade(CanvasGroup canvGroup, float start, float end)
    {
        float counter = 0f;
        while(counter < duration)
        {
            counter += Time.deltaTime;
            canvGroup.alpha = Mathf.Lerp(start, end, counter / duration);
            yield return null;
        }
    }
}