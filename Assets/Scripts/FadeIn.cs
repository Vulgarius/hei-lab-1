﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeIn : MonoBehaviour
{
    public bool _mFaded;
    public float duration;
    public GameObject titleMenu;
    public void Update()
    {
        if (_mFaded == false && titleMenu) { 
            var canvGroup = GetComponent<CanvasGroup>();
            StartCoroutine(DoFade(canvGroup, canvGroup.alpha, _mFaded ? 0 : 1));
            _mFaded = !_mFaded;
        }
    }

    public IEnumerator DoFade(CanvasGroup canvGroup, float start, float end)
    {
        float counter = 0f;
        while(counter < duration)
        {
            counter += Time.deltaTime;
            canvGroup.alpha = Mathf.Lerp(start, end, counter / duration);
            yield return null;
        }
    }
}