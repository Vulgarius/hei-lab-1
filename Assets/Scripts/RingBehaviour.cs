﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingBehaviour : MonoBehaviour
{

    public Transform targetring;

    public float rotationSpeed = 30;

    public GameObject particles;
    
    private AudioSource _audioSource;

    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.transform.position.y < player.transform.position.y + 6 && gameObject.transform.position.y > player.transform.position.y - 6)
            Rotate();
    }

    void Rotate()
    {
        transform.Rotate(0,0,1*rotationSpeed*Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && this.gameObject.name != "Origin")
        { 
            _audioSource.Play();
            Explode();
        }
    }

    void Explode()
    {
        GetComponentInChildren<ParticleSystem>().Play();
    }
}
