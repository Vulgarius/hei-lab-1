using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalQuit : MonoBehaviour
{

    public GameObject thanks;
    public CanvasGroup menu3;

    public void Quitter()
    {
        StartCoroutine(Quit());
    }
    
    private IEnumerator Quit()
    {
        menu3.alpha = 0;
        
        thanks.SetActive(true);
        thanks.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(5);
        thanks.GetComponent<FadeIn>().enabled = false;
        thanks.GetComponent<FadeIn>()._mFaded = false;
        
        Application.Quit();
    }
    
}
